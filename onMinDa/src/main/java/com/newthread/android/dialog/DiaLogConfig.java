package com.newthread.android.dialog;

import com.newthread.android.dialog.dialoginformation.DiaLogType;

import java.io.Serializable;


public class DiaLogConfig implements Serializable{
	  public static final String INTENT_EXTRA_CONFIG_NAME = "DIALOG_INTENT_EXTRA_CONFIG_NAME";

	  private static final long serialVersionUID = 1L;

	private DiaLogType diaLogType;
	
	public int getRequestCode(){
		return diaLogType.getRequestCode();
	}
	public DiaLogType getDiaLogType() {
		return diaLogType;
	}
	public Class<?> getDialogClassType(){
		return diaLogType.getDialogClassType();
	}
	protected DiaLogConfig(DiaLogType diaLogType) {
		this.diaLogType = diaLogType;
	}
	
}
