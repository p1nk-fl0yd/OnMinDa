package com.newthread.android.dialog.dialoginformation;


import com.newthread.android.dialog.DiaLogConfig;

public class PalyMusicDiaLogConfig extends DiaLogConfig {

	 public static final int RESULT_CODE_CONFIRM = 0;
	  public static final int RESULT_CODE_CANCEL = RESULT_CODE_CONFIRM + 1;
	
	private static final long serialVersionUID = 1L;
	private String information;

	public PalyMusicDiaLogConfig(String information) {
		super(DiaLogType.CONFIRM);
		this.information=information;
	}

	public String getInformation() {
		return information;
	}
}
