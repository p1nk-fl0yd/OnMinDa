package com.newthread.android.camera;

public class StartSrerviceState extends ServiceState {

	
	static class StartSrerviceStateHolder{
		static  StartSrerviceState instance = new StartSrerviceState();
	}
	public static  StartSrerviceState getInstance(){
		return StartSrerviceStateHolder.instance;
	}
	public StartSrerviceState() {
		
		this.setIsaLive(true);
	}
}
