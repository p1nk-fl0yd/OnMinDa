package com.newthread.android.manager;


import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.newthread.android.R;
import com.newthread.android.activity.main.OnCampusActivity;
import com.newthread.android.bean.SingleCourseInfo;
import com.newthread.android.clock.TimeRemindService;
import com.newthread.android.dialog.DiaLogManager;
import com.newthread.android.dialog.dialoginformation.PalyMusicDiaLogConfig;
import com.newthread.android.manager.iMangerService.IRemindService;
import com.newthread.android.util.TimeUtil;
import java.util.List;

/**
 * Created by Administrator on 2014/8/26.
 */
public class CourseRemindManger implements IRemindService<SingleCourseInfo> {
    private Context context;
    private static CourseRemindManger instance;

    private CourseRemindManger(Context context) {
        this.context = context;
    }

    public static CourseRemindManger getInstance(Context context) {
        synchronized (CourseRemindManger.class) {
            if (instance == null) {
                instance = new CourseRemindManger(context);
            }
        }
        return instance;
    }


    @Override
    public void openAllRemind() {

    }

    @Override
    public void closeALlRemind() {

    }

    @Override
    public void openRemind(SingleCourseInfo singleCourseInfo) {
        String name = singleCourseInfo.getCourseName();
        String time = TimeUtil.getTimeFromCourse(singleCourseInfo, 15);
        Bundle bundle = new Bundle();
        bundle.putString("name", name);
        bundle.putString("time", time);
        Intent intent = new Intent(context, TimeRemindService.class);
        intent.putExtras(bundle);
        context.startService(intent);
    }

    @Override
    public void closeRemind(SingleCourseInfo singleCourseInfo) {

    }

    @Override
    public void openSeletedRemind(List<SingleCourseInfo> t) {

    }

    @Override
    public void closeSeletedRemind(List<SingleCourseInfo> t) {

    }

    @Override
    public void creatNotiforcation(Intent _intent) {
        Notification notification = new Notification();
        notification.icon = R.drawable.notify;
        notification.when = System.currentTimeMillis();
//        notification.sound = Uri.parse("android.resource://" + context.getPackageName() + File.separator + R.raw.order);
        notification.tickerText = "要上课啦！";
        notification.flags |= Notification.FLAG_AUTO_CANCEL;
        notification.defaults |= Notification.DEFAULT_VIBRATE;
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        Intent intent = new Intent(context, OnCampusActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
        notification.contentIntent = pendingIntent;
        notification.setLatestEventInfo(context, "人在民大", _intent.getExtras().getString("name"), pendingIntent);
        NotificationManager notificationManager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.notify(1, notification);
        DiaLogManager.showDiaLog(context,null,new PalyMusicDiaLogConfig(_intent.getExtras().getString("name")));
    }


}
