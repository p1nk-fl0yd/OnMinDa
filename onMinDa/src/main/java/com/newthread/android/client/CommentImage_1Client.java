package com.newthread.android.client;

import android.content.Context;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.datatype.BmobPointer;
import cn.bmob.v3.listener.DeleteListener;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.bean.remoteBean.bbs.CommentImage_1;
import com.newthread.android.manager.iMangerService.IRemoteService;
import com.newthread.android.util.Loger;

/**
 * Created by jindongping on 15/6/20.
 */
public class CommentImage_1Client implements IRemoteService<CommentImage_1> {
    private Context context;
    public CommentImage_1Client(Context context) {
        this.context=context;
    }

    @Override
    public void add(CommentImage_1 commentImage_1, SaveListener saveListener) {
        if(commentImage_1.getComment()==null){
            Loger.V("评论的对象为空");
            return;
        }
        commentImage_1.save(context,saveListener);
    }

    @Override
    public void update(CommentImage_1 commentImage_1, UpdateListener updateListener) {

    }

    @Override
    public void querry(CommentImage_1 commentImage_1, FindListener<CommentImage_1> findListener) {
        if(commentImage_1.getComment()==null){
            Loger.V("评论的对象为空");
            return;
        }
        BmobQuery<CommentImage_1> query = new BmobQuery<>();
        query.addWhereEqualTo("comment",new BmobPointer(commentImage_1.getComment()));
        query.findObjects(context, findListener);
    }

    @Override
    public void delete(CommentImage_1 commentImage_1, DeleteListener deleteListener) {

    }
}
