package com.newthread.android.view.bbsview;


import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import com.newthread.android.R;

public class MyRelativeLayout extends RelativeLayout implements
		OnClickListener {

	public MyRelativeLayout(Context context) {
		super(context);
		init();
	}

	public MyRelativeLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
		init();
	}

	private void init() {
		setBackgroundResource(R.drawable.list_item_selector);
		setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
	}

	@Override
	public boolean hasFocusable() {
		return false;
	}

}
