package com.newthread.android.bean.remoteBean.bbs;

import android.content.Context;
import cn.bmob.v3.datatype.BmobRelation;
import com.newthread.android.bean.remoteBean.RemoteObject;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.client.PostClient;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/5/29.
 */
public class Post extends RemoteObject {
    private String title;//帖子标题

    private String content;// 帖子内容

    private StudentUser author;//帖子的发布者，这里体现的是一对一的关系，一个用户发表一个帖子

    private BmobRelation likes;//多对多关系：用于存储喜欢该帖子的所有用户

    private PostType type; //种类

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public StudentUser getAuthor() {
        return author;
    }

    public void setAuthor(StudentUser author) {
        this.author = author;
    }

    public BmobRelation getLikes() {
        return likes;
    }

    public void setLikes(BmobRelation likes) {
        this.likes = likes;
    }

    public PostType getType() {
        return type;
    }

    public void setType(PostType type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Post{" +
                "id =" + getObjectId() +
                "title='" + title + '\'' +
                ", content='" + content + '\'' +
                ", authorIsNull?" + (author == null) +
                ", typeIsNull?" + (type == null) +
                '}';
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;

        final Post other = (Post) obj;
        if (!this.getTitle().equals(other.getTitle()))
            return false;
        if (!this.getContent().equals(other.getContent()))
            return false;
        if (!this.getCreatedAt().equals(other.getCreatedAt()))
            return false;

        return true;
    }

    @Override
    public IRemoteService getImpl(Context context) {
        return new PostClient(context);
    }
}
