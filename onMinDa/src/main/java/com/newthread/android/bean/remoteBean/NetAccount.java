package com.newthread.android.bean.remoteBean;

import android.content.Context;
import com.newthread.android.client.AccoutNetSharedClient;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 15/5/27.
 */
public class NetAccount extends RemoteObject{
    private String studentId;
    private String passWord;
    private boolean isUsing = false;
    private boolean isSharing = true;

    public String getStudentId() {
        return studentId;
    }

    public void setStudentId(String studentId) {
        this.studentId = studentId;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public boolean isUsing() {
        return isUsing;
    }

    public void setUsing(boolean isUsing) {
        this.isUsing = isUsing;
    }

    public boolean isSharing() {
        return isSharing;
    }

    public void setSharing(boolean isSharing) {
        this.isSharing = isSharing;
    }

    public NetAccount(String studentId, String passWord) {
        this.studentId = studentId;
        this.passWord = passWord;
    }

    @Override
    public IRemoteService getImpl(Context context) {
        return new AccoutNetSharedClient(context);
    }
}
