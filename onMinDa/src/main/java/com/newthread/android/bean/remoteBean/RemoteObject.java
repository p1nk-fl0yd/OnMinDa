package com.newthread.android.bean.remoteBean;

import android.content.Context;
import cn.bmob.v3.BmobObject;
import com.newthread.android.manager.iMangerService.IRemoteService;

/**
 * Created by jindongping on 14/12/9.
 * 这个类设计不应该继承BmobObject，但是由于Bomb设计问题只能继承才能使用它的save方法
 */
public abstract class RemoteObject extends BmobObject {
    public abstract IRemoteService getImpl(Context context);
}
