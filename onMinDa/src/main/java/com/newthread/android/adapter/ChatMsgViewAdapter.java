package com.newthread.android.adapter;

import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.newthread.android.R;
import com.newthread.android.bean.bbs.ChatMsgEntity;
import com.newthread.android.ui.DisPalyVPActivity;
import com.newthread.android.ui.bbs.BbsChatActivity;
import com.newthread.android.util.ImageUtil;
import com.newthread.android.util.Loger;
import net.tsz.afinal.FinalActivity;
import net.tsz.afinal.annotation.view.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class ChatMsgViewAdapter extends BaseAdapter {

    public static interface IMsgViewType {
        int IMVT_COM_MSG = 0;
        int IMVT_TO_MSG = 1;
    }

    private static final String TAG = ChatMsgViewAdapter.class.getSimpleName();

    private List<ChatMsgEntity> coll;

    private Context ctx;

    private LayoutInflater mInflater;
    private MediaPlayer mMediaPlayer = new MediaPlayer();

    public ChatMsgViewAdapter(Context context, List<ChatMsgEntity> coll) {
        ctx = context;
        this.coll = coll;
        mInflater = LayoutInflater.from(context);
    }

    public int getCount() {
        return coll.size();
    }

    public Object getItem(int position) {
        return coll.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public int getItemViewType(int position) {
        ChatMsgEntity entity = coll.get(position);

        if (entity.getMsgType()) {
            return IMsgViewType.IMVT_COM_MSG;
        } else {
            return IMsgViewType.IMVT_TO_MSG;
        }

    }

    public int getViewTypeCount() {
        return 2;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        final ChatMsgEntity entity = coll.get(position);
        boolean isComMsg = entity.getMsgType();

        ViewHolder viewHolder = null;
        if (convertView == null) {
            if (isComMsg) {
                convertView = mInflater.inflate(R.layout.bbs_chatting_item_msg_text_left, parent, false);
            } else {
                convertView = mInflater.inflate(R.layout.bbs_chatting_item_msg_text_right, parent, false);
            }
            viewHolder = new ViewHolder(convertView);
            viewHolder.isComMsg = isComMsg;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.tvSendTime.setText(entity.getDate());
        if (entity.getText().contains(".amr")) {
            viewHolder.tvContent.setText("");
            viewHolder.tvContent.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.chatto_voice_playing, 0);
            viewHolder.tvTime.setText(entity.getTime());
        } else {
            viewHolder.tvContent.setText(entity.getText());
            viewHolder.tvContent.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            viewHolder.tvTime.setText("");
        }
        viewHolder.tvContent.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                if (entity.getText().contains(".amr")) {
                    if (entity.getMsgType()) {
                        //别人发得语音
                        playMusic(entity.getText());
                    } else {
                        //自己发过去的语言
                        playMusic(BbsChatActivity.VOIC_PATH + "/" + entity.getText());
                    }
                }
            }
        });
        viewHolder.headPhoto.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> list = new ArrayList<>();
                list.add(entity.getHeadPhoto());
                Bundle bundle = new Bundle();
                bundle.putStringArrayList("images", list);
                bundle.putInt("selectImagePosition", 0);
                bundle.putString("loadImageEngine", "imageLoader");
                Intent intent = new Intent(mInflater.getContext(), DisPalyVPActivity.class);
                intent.putExtras(bundle);
                mInflater.getContext().startActivity(intent);
            }
        });
        viewHolder.tvUserName.setText(entity.getName());
        ImageUtil.getInstance(mInflater.getContext()).disPalyImage(entity.getHeadPhoto(), viewHolder.headPhoto);
        return convertView;
    }

    static class ViewHolder {
        @ViewInject(id = R.id.tv_sendtime)
        public TextView tvSendTime;
        @ViewInject(id = R.id.tv_username)
        public TextView tvUserName;
        @ViewInject(id = R.id.tv_chatcontent)
        public TextView tvContent;
        @ViewInject(id = R.id.tv_time)
        public TextView tvTime;
        @ViewInject(id = R.id.iv_userhead)
        public ImageView headPhoto;
        public boolean isComMsg = true;

        public ViewHolder(View view) {
            FinalActivity.initInjectedView(this, view);
        }
    }

    /**
     * @param name
     * @Description
     */
    private void playMusic(String name) {
        try {
            if (mMediaPlayer.isPlaying()) {
                mMediaPlayer.stop();
            }
            mMediaPlayer.reset();
            mMediaPlayer.setDataSource(name);
            mMediaPlayer.prepare();
            mMediaPlayer.start();
            mMediaPlayer.setOnCompletionListener(new OnCompletionListener() {
                public void onCompletion(MediaPlayer mp) {

                }
            });

        } catch (Exception e) {
            e.printStackTrace();
            Loger.V(e);
        }

    }

    private void stop() {

    }

}
