package com.newthread.android.client;

import android.os.SystemClock;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.LargeTest;
import cn.bmob.v3.BmobQuery;
import cn.bmob.v3.listener.FindListener;
import cn.bmob.v3.listener.SaveListener;
import cn.bmob.v3.listener.UpdateListener;
import com.newthread.android.activity.main.MyApplication;
import com.newthread.android.bean.remoteBean.StudentUser;
import com.newthread.android.bean.remoteBean.bbs.PostType;
import com.newthread.android.manager.BmobRemoteDateManger;
import com.newthread.android.util.Loger;

import java.util.List;

/**
 * Created by jindongping on 15/6/1.
 */
public class PostTypeClientTest extends ApplicationTestCase<MyApplication> {


    public PostTypeClientTest(Class<MyApplication> applicationClass) {
        super(MyApplication.class);
    }

    public PostTypeClientTest() {
        super(MyApplication.class);
    }


    @Override
    public void setUp() throws Exception {
        super.setUp();
        createApplication();
    }

    /**
     * 增加帖子类型
     *
     * @throws Exception
     */
    @LargeTest
    public void testAdd() throws Exception {
        PostType postType = new PostType();
        postType.setType("test");
        BmobRemoteDateManger.getInstance(getApplication()).add(postType, new SaveListener() {
            @Override
            public void onSuccess() {
                Loger.V("sucess");
            }

            @Override
            public void onFailure(int i, String s) {
                Loger.V("fail" + i + s);
            }
        });

        SystemClock.sleep(4000);
    }

    /**
     * 更新帖子类型收藏者
     *
     * @throws Exception
     */
    @LargeTest
    public void testUpdate() throws Exception {
        StudentUser user = new StudentUser(null, null);
        user.setObjectId("5577345bc8");

        PostType postType = new PostType();
        postType.setObjectId("b1e7820096");
        postType.setLikes(user);

        BmobRemoteDateManger.getInstance(getApplication()).update(postType, new UpdateListener() {
            @Override
            public void onSuccess() {
                Loger.V("sucess");
            }

            @Override
            public void onFailure(int i, String s) {
                Loger.V("fail" + i + s);
            }
        });

        SystemClock.sleep(4000);
    }

    /**
     * 查询所有帖子种类
     * @throws Exception
     */
    @LargeTest
    public void testQuerry() throws Exception {
        BmobQuery<PostType> query = new BmobQuery<>();
        query.findObjects(getApplication(), new FindListener<PostType>() {
            @Override
            public void onSuccess(List<PostType> list) {
                for(PostType postType:list){
                    Loger.V(postType);
                }
            }

            @Override
            public void onError(int i, String s) {

            }
        });
        SystemClock.sleep(4000);
    }
}